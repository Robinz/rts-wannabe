package org.academiadecodigo.robin.rts;

import org.academiadecodigo.robin.rts.building.BuildingType;
import org.academiadecodigo.robin.rts.unit.Worker;

public class Main {

    public static void main(String[] args) {

        Worker worker = new Worker();

        worker.build(BuildingType.STRONGHOLD);


    }
}
