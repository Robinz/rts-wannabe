package org.academiadecodigo.robin.rts.spell;

import org.academiadecodigo.robin.rts.unit.Unit;

public abstract class Spell implements Castable {


    @Override
    public abstract void cast(Unit unit, int spellDamageMultiplier);
}
