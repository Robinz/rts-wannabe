package org.academiadecodigo.robin.rts.building;

import org.academiadecodigo.robin.rts.damageInterfaces.Hittable;
import org.academiadecodigo.robin.rts.damageInterfaces.Offensive;

public class Tower extends Building implements Offensive {

    private int damage;

    public Tower() {
        super(300);
        damage = 30;
    }

    @Override
    public void attack(Hittable hittable) {
        hittable.suffer(damage);
    }
}
