package org.academiadecodigo.robin.rts.building;

import org.academiadecodigo.robin.rts.damageInterfaces.Hittable;


public abstract class Building implements Hittable, Buildable {

    private static int maxDurability;
    protected int durability;
    private boolean built;

    public Building(int maxDurability){
        this.maxDurability = maxDurability;
        durability = 0;
        built = false;
    }

    @Override
    public void suffer(int damage) {
        durability -= damage;
    }

    protected static int getMaxDurability(){
        return maxDurability;
    }

    //TODO THIS SOLUTION IS A MESS
    @Override
    public void build(int buildingSpeed){

        while (durability<= maxDurability){
            durability +=buildingSpeed;

            try {

                Thread.sleep(10);
            } catch (InterruptedException e) {

                e.printStackTrace();
            }
            System.out.println("Durability : " + durability);
        }

        System.out.println(durability);

    }

    public int getDurability() {
        return durability;
    }
}
