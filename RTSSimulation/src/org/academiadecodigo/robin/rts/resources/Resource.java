package org.academiadecodigo.robin.rts.resources;

public abstract class Resource implements Collectable {

    protected int currentAmount;

    public int collect(int amountCollected){

        if (currentAmount > 0){

            currentAmount -= amountCollected;
        }

        return amountCollected;

    }
}
